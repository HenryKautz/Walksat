Walksat - Version 53
March 2018
Henry Kautz <henry.kautz@gmail.com>
https://gitlab.com/HenryKautz/Walksat

Compilation

- Edit flags at beginning of walksat.c to change the following
options:

    Exactly one of the following should be defined to be 1, 
    the rest defined to be 0:
	UNIX (Most versions of Unix, including Linux and Mac OS X)
        NT   (Microsoft Windows)
        ANSI (Ansi Unix, use only if will not compile under Unix)

    Constants that control size of problem that can be handled:

        MAXATOM - hard limit on the number of atoms. If your problem
	    is bigger, increase and recompile.

	MAXLENGTH - hard limit on number of literals in a clause.

        DYNAMIC - use dynamic memory for storing clauses.  Set to false
	    to allow more compiler optimizations.
	
        MAXCLAUSE - limit on number of clauses if DYNAMIC is false.

- For Windows, create a workspace/project and add the library
Winmm.lib to the "linker" tab.

Compilation options:
	make           # compile everything
	make install   # compile and copy to $USER/bin
	make clean     # remove .o files

*****************************************************************

Walksat specifications

Walksat attempts to find a satisfying model of a cnf formula.
Format is .cnf format:

c Optional comments at start of file.
c The "p" line specifies cnf format, number vars, number clauses
c Variables are numbered starting at 1
c Each clause ends with 0
p cnf 3 2
1 -3 0
2 3 -1 0

Walksat reads from standard in or from a file specified as a command
line argument and writes to standard out and standard error.  To
get a list of command line options, type
	walksat -help
or any illegal option.

*****************************************************************

Also included in distribution:

makewff [-seed N] [-cnf] [-f] [-kf] clen nvars nclauses
Generate random formulas using the fixed clause length model.
The -seed argument can be used to initialize the random number
generator.  The -cnf, -f, and -kf options specify the format of the
wff file; for walksat use -cnf.  Clen is the length of each clause,
nvars the number of variables, and nclauses the number of clauses.

makequeens N
Generate a N-queens formula.

*****************************************************************

HISTORY OF MAJOR RELEASES

version 07 - introduced breakcount[] array
version 14 - added -super option
version 16 - improved print options and help message
version 17 - added trace N option
version 25 - -hamming option
version 28 - novelty and rnovelty heuristics
version 41 - use long long ints for cutoff
version 44 - special version for sat competition - not distributed
version 45 - watched literals rule added
version 48 - allow cnf file name to appear as argument on command line
version 49 - various minor bugs, watched literals rule removed
version 50 - code optimizations suggested by Don Knuth
version 53 - added -plus option for -novelty and -rnovelty
             fixed -tabu;
	     added heuristic -alternate;
	     added heuristic -bigflip;
	     added -adaptive noise option;
	     added -nofreebie option;
	     added -maxfreebie option;
	     added -walkprob R to substitute for -noise N M option;
	     replaced macro functions with inline functions;
	     added directory of Papers to distribution;
	     moved distribution to gitlab.com.


